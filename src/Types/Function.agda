module Types.Function where

open import CommonCubical
open import Classes.Cubical.Profunctor
open import Classes.Cubical.Functor

private
  variable
    ℓ ℓ′ : Level
    A : Type ℓ

infixr -1 _⇾_
_⇾_ : Type ℓ → Type ℓ′ → Type (ℓ ⊔ ℓ′)
a ⇾ b = a → b

⇾-Functor : IsFunctor {ℓ} (A ⇾_) _∘′_
⇾-Functor = record {
  fmap-id = refl
  ; fmap-∘ = refl }

instance
  ⇾-Profunctor : Profunctor {ℓ} {ℓ′} (_⇾_)
  ⇾-Profunctor = record
    { dimap = λ ab cd bc → cd ∘ bc ∘ ab ;
    dimap-id = refl ;
    dimap-∘ = refl }
