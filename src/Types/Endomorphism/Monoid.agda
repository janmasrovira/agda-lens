open import CommonCubical
module Types.Endomorphism.Monoid where

open import Function.Endomorphism.Propositional using (Endo)
open import Classes.Cubical.Monoid
open import Classes.Cubical.Semigroup

private
  variable
    a ℓ : Level
    A : Type ℓ

endo-semigroup : IsSemigroup {A = Endo A} _≡_ _∘′_
endo-semigroup =
    record {
     isMagma = record { isEquivalence = ≡-IsEquivalence
     ; ∙-cong = λ {v} {u} {y} {x} vu yx i a → vu i (yx i a) }
     ; assoc = λ f g h i x → f (g (h x)) }

endo-monoid : IsMonoid {A = Endo A} _≡_ _∘′_ id
endo-monoid = record
  { isSemigroup = endo-semigroup
  ; identity = (λ _ → refl) , λ x → refl }
