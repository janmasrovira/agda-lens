module Types.Compose where

open import CommonCubical
open import Classes.Cubical.Functor

private
  variable
    ℓ : Level
    ℓb ℓa ℓc : Level
    F G : Type ℓa → Type ℓb
    f g : ∀ {A B} → (A → B) → F A → F B

composeFunctor : IsFunctor F f → IsFunctor G g
  → IsFunctor (F ∘ G) (f ∘ g)
composeFunctor {f = f} {g = g} 𝔽 𝔾 = record
 { fmap-id = fmap-id
 ; fmap-∘ = fmap-∘ }
 where
 fmap-id : ∀ {A} → (f ∘ g {A}) id ≡ id
 fmap-id = (f ∘ g) id ≡⟨ refl ⟩
            f (g id) ≡⟨ cong f (IsFunctor.fmap-id 𝔾) ⟩
            f id     ≡⟨ IsFunctor.fmap-id 𝔽 ⟩
            id ∎
 fmap-∘ : ∀ {B C A} {a : B → C} {b : A → B} →
      (f ∘ g) (a ∘ b) ≡ (f ∘ g) a ∘ (f ∘ g) b
 fmap-∘ {a = a} {b = b} =
    (f ∘ g) (a ∘ b) ≡⟨ refl ⟩
    f (g (a ∘ b)) ≡⟨ cong f (IsFunctor.fmap-∘ 𝔾) ⟩
    f (g a ∘ g b) ≡⟨ IsFunctor.fmap-∘ 𝔽 ⟩
    (f (g a) ∘ f (g b)) ≡⟨ refl ⟩
    (f ∘ g) a ∘ (f ∘ g) b
    ∎
