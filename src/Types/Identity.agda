module Types.Identity where

open import Classes.Cubical.Functor
open import CommonCubical
open import Function.Identity.Categorical using (Identity) public

private
  variable
    ℓ : Level

Identity-Functor : IsFunctor {ℓ} Identity id
Identity-Functor = record { fmap-id = refl ; fmap-∘ = refl }
