module Types.Maybe where

open import Classes.Cubical.Functor
open import Classes.Cubical.Applicative
open import CommonCubical

private
  variable
    ℓ : Level
    A B C : Type ℓ

data Maybe (A : Type ℓ) : Type ℓ where
  nothing : Maybe A
  just : A → Maybe A

fmap : (A → B) → Maybe A → Maybe B
fmap f nothing = nothing
fmap f (just x) = just (f x)

id-maybe : fmap {A = A} id ≡ id
id-maybe i nothing = nothing
id-maybe i (just x) = just x

∘-maybe : {f : B → C} {g : A → B} →
  fmap (f ∘ g) ≡ (fmap f ∘ fmap g)
∘-maybe i nothing = nothing
∘-maybe {f = f} {g = g} i (just x) = just (f (g x))

Maybe-Functor : IsFunctor (Maybe {ℓ}) fmap
Maybe-Functor = record
  { fmap-id = id-maybe
  ; fmap-∘ = ∘-maybe }

_⟨*⟩_ : Maybe (A → B) → Maybe A → Maybe B
nothing ⟨*⟩ _ = nothing
just f ⟨*⟩ x = fmap f x

Maybe-Applicative : IsApplicative {ℓ} Maybe fmap just _⟨*⟩_
Maybe-Applicative = record
  { functor = Maybe-Functor
  ; identity =
    λ {_} {v} →
    fmap id v ≡⟨ funExt⁻ id-maybe _ ⟩
    v ∎
  ; composition = λ {_} {_} {_} {u} → composition {u = u}
  ; homomorphism = refl
  ; interchange = λ { {_} {_} {nothing} {_} → refl
    ; {_} {_} {just x} {_} → refl}
  }
  where
  composition : {u : Maybe (B → C)} {v : Maybe (A → B)}
      {w : Maybe A} → ((fmap _∘′_ u ⟨*⟩ v) ⟨*⟩ w) ≡ (u ⟨*⟩ (v ⟨*⟩ w))
  composition {u = nothing} {v = v} {w = w} = refl
  composition {u = just x} {v = nothing} {w = w} = refl
  composition {u = just x} {v = just x₁} {w = w} = funExt⁻ ∘-maybe _
