module Types.Const where

open import Classes.Cubical.Functor
open import CommonCubical

private
  variable
    ℓ ℓ′ : Level
    A : Type ℓ

Const : Type ℓ → Type ℓ′ → Type ℓ
Const A _ = A

Const-Functor : Functor (Const {ℓ} {ℓ′} A)
Const-Functor = record { fmap = λ f x → x
  ; isFunctor = record { fmap-id = refl
  ; fmap-∘ = refl } }
