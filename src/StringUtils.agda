module _ where

open import Common
open import Data.String using (String; _++_) public
open import Agda.Builtin.String

private
  variable
   ℓ : Level
   A : Type ℓ

brackets : String → String
brackets s = "[" ++ s ++ "]"

sepBy : String → List String → String
sepBy s [] = ""
sepBy s (x ∷ []) = x
sepBy s (x ∷ t@(_ ∷ _)) = x ++ s ++ sepBy s t

commaSep : List String → String
commaSep = sepBy ", "

showList : (A → String) → List A → String
showList f = brackets ∘ commaSep ∘ map f

showℕList : List ℕ → String
showℕList = showList primShowNat

record Show {ℓ} (A : Type ℓ) : Type ℓ where
  field
    show : A → String

open Show {{...}} public

instance
  Show-ℕ : Show ℕ
  Show-ℕ = record { show = primShowNat }

instance
  Show-List : {A : Type ℓ} → ⦃ Show A ⦄ → Show (List A)
  Show-List = record { show = showList show }
