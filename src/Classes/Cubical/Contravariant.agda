module Classes.Cubical.Contravariant where

open import CommonCubical

private
  variable
    ℓ ℓ′ : Level
    A B C : Type ℓ

record Contravariant (F : Type ℓ → Type ℓ′) : Type (ℓ-suc ℓ ⊔ ℓ′) where
  field
    contramap : (A → B) → F B → F A

    identity : (x : F A) → (contramap id x) ≡ x
    composition : (x : F C) (f : B → C) (g : A → B) → (contramap (f ∘ g) x) ≡ (contramap g ∘ contramap f) x
