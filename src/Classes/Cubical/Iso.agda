module Classes.Cubical.Iso where

open import CommonCubical
open import Classes.Cubical.Functor
open import Classes.Cubical.Profunctor

private
  variable
    ℓ ℓ′ ℓ″ : Level
    ℓt : Level

Iso : {s : Type ℓ} {t : Type ℓt} {a  : Type ℓ} {b : Type ℓt}
  → (P : Type ℓ → Type ℓ′ → Type ℓ″) → Profunctor P → (F : Type _ → Type _) → Functor F → Type _
Iso {s = s} {t = t} {a = a} {b = b} P _ F _ = Profunctor P → Functor F → P a (F b) → P s (F t)
