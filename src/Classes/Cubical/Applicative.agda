module Classes.Cubical.Applicative where

open import CommonCubical
open import Classes.Cubical.Functor

private
  variable
    ℓ ℓ′ : Level
    A B C : Type ℓ

record IsApplicative
  (F : Type ℓ → Type ℓ′)
  (fmap : ∀ {A B} → (A → B) → F A → F B)
  (pure : ∀ {A} → A → F A)
  (_⟨*⟩_ : ∀ {A B} → F (A → B) → F A → F B)
  : Type (ℓ-suc ℓ ⊔ ℓ′) where
  field
    functor : IsFunctor F fmap

    identity : {v : F A} → (pure id ⟨*⟩ v) ≡ v
    composition : ∀ {u : F (B → C)} {v : F (A → B)} {w : F A}
       → (((pure _∘′_ ⟨*⟩ u) ⟨*⟩ v) ⟨*⟩ w) ≡ (u ⟨*⟩ (v ⟨*⟩ w))
    homomorphism : {f : A → B} {x : A} → (pure f ⟨*⟩ pure x) ≡ (pure (f x))
    interchange : {u : F (A → B)} {y : A} → (u ⟨*⟩ pure y) ≡ (pure (_$ y) ⟨*⟩ u)

record Applicative (F : Type ℓ → Type ℓ′) : Type (ℓ-suc ℓ ⊔ ℓ′) where
  infixl 4 _⟨*⟩_
  field
   fmap : ∀ {A B} → (A → B) → F A → F B
   isFunctor : IsFunctor F fmap

   pure : ∀ {A} → A → F A
   _⟨*⟩_ : ∀ {A B} → F (A → B) → F A → F B
   isApplicative : IsApplicative F fmap pure _⟨*⟩_
