module Classes.Cubical.Monoid where

open import CommonCubical
open import Algebra.Structures using (IsMonoid) public

private
  variable
    ℓ : Level

record Monoid (A : Type ℓ) : Type (ℓ-suc ℓ) where
  field
   _<>_ : A → A → A
   ε : A
   monoid : IsMonoid _≡_ _<>_ ε
