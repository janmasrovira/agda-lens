module Classes.Cubical.Profunctor where

open import CommonCubical

private
  variable
    ℓ ℓ′ ℓ″ : Level
    A B C D : Type ℓ
    A' B' C' : Type ℓ

record Profunctor (P : Type ℓ → Type ℓ′ → Type ℓ″) : Type (ℓ-suc ℓ ⊔ ℓ-suc ℓ′ ⊔ ℓ″) where
  field
    dimap : (A → B) → (C → D) → P B C → P A D
    dimap-id : dimap {A} {A} {C} id id ≡ id
    dimap-∘ : ∀ {f : B → C} {g : A → B} {h : A' → B'} {i : C' → A'} → dimap (f ∘ g) (h ∘ i) ≡ dimap g h ∘ dimap f i

  lmap : (A → B) → P B C → P A C
  lmap f p = dimap f id p

  rmap : (B → C) → P A B → P A C
  rmap f p = dimap id f p
