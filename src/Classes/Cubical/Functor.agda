module Classes.Cubical.Functor where

open import CommonCubical

private
  variable
    ℓ ℓ′ : Level
    A B C : Type ℓ

record IsFunctor (F : Type ℓ → Type ℓ′)
                 (fmap : ∀ {A B} → (A → B) → F A → F B)
                 : Type (ℓ-suc ℓ ⊔ ℓ′) where
  field

    fmap-id : fmap {A} id ≡ id
    fmap-∘ : {f : B → C} {g : A → B} → fmap (f ∘ g) ≡ fmap f ∘ fmap g


record Functor (F : Type ℓ → Type ℓ′) : Type (ℓ-suc ℓ ⊔ ℓ′) where
  field
    fmap : ∀ {A B} → (A → B) → F A → F B
    isFunctor : IsFunctor F fmap

  infixl 4 _<$>_
  _<$>_ = fmap
