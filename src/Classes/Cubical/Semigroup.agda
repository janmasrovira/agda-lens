module Classes.Cubical.Semigroup where

open import CommonCubical
open import Algebra.Structures using (IsSemigroup) public

private
  variable
    ℓ ℓ′ ℓ″ : Level

record Semigroup (A : Type ℓ) : Type (ℓ-suc ℓ) where
  field
   _<>_ : A → A → A
   semigroup : IsSemigroup _≡_ _<>_
