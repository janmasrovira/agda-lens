module Classes.Cubical.Monad where

open import CommonCubical
open import Classes.Cubical.Applicative

private
  variable
    ℓ ℓ′ : Level
    A B C : Type ℓ

record IsMonad
  (F : Type ℓ → Type ℓ′)
  (fmap : ∀ {A B} → (A → B) → F A → F B)
  (pure : ∀ {A} → A → F A)
  (_⟨*⟩_ : ∀ {A B} → F (A → B) → F A → F B)
  (_>>=_ : ∀ {A B} → F A -> (A → F B) → F B)
  : Type (ℓ-suc ℓ ⊔ ℓ′) where
  return : A → F A
  return = pure
  field
    applicative : IsApplicative F fmap pure _⟨*⟩_

    left-id : {a : A} {k : A → F B} → return a >>= k ≡ k a
    right-id : {h : F A} → h >>= return ≡ h
    assoc : {m : F A} {k : A → F B} {h : B → F C}
      → m >>= (λ x → k x >>= h) ≡ (m >>= k) >>= h
