module Classes.Foldable where

open import Common
open import Classes.Monoid
open import Types.Endomorphism.Monoid
open import Classes.Functor
open import Function.Endomorphism.Propositional using (∘-id-isMonoid)

private
  variable
    ℓ ℓ′ ℓ″ : Level
    ℓs ℓt ℓb ℓa ℓr ℓf1 ℓf2 : Level
    a b c : Type ℓ

module _ (F : Type ℓ → Type ℓ) where

  module FoldableOps (foldr : {a b : Type ℓ} → (a → b → b) → b → F a → b) where

    module _ {m} (mon : Monoid {ℓ} m) where
      open Monoid mon

      fold : F m → m
      fold = foldr _∙_ ε

      module _ (fun : Functor F) where
        open Functor fun

        foldMap : (a → m) → F a → m
        foldMap f = fold ∘ fmap f


  record Foldable : Type (ℓ-suc ℓ) where
    field
      foldr : {a b : Type ℓ} → (a → b → b) → b → F a → b

    open FoldableOps foldr

    field
      foldMap-id : ∀ {m} → (mon : Monoid m) → (fun : Functor F)
        → ∀ {l} → fold mon l ≡ foldMap mon fun id l
      foldr-∘ : (fun : Functor F) → ∀ {f : a → b → b} {z : b} {t}
       → foldr f z t ≡ (foldMap (endo-monoid b) fun f t) z
