module Classes.Traversable where

open import Common
open import Classes.Functor
open import Classes.Foldable
open import Classes.Applicative

private
  variable
    ℓ ℓ′ : Level
    a b : Type ℓ

record ApplicativeTransform {F : Type ℓ → Type ℓ} {G : Type ℓ → Type ℓ}
   (t : ∀ {a : Type ℓ} → F a → G a) : Type (ℓ-suc ℓ) where
  field
    app-F : IsApplicative F
    app-G : IsApplicative G
  open IsApplicative app-F renaming (pure to Fpure; _⟨*⟩_ to _F⟨*⟩_)
  open IsApplicative app-G renaming (pure to Gpure; _⟨*⟩_ to _G⟨*⟩_)
  field
    t1 : ∀ {A : Type ℓ} {a : A} → t (Fpure a) ≡ Gpure a
    t2 : ∀ {x} {f : F (a → a)} → (t (f F⟨*⟩ x)) ≡ (t f G⟨*⟩ t x)


record Traversable (T : Type ℓ → Type ℓ) : Type (ℓ-suc ℓ) where
  field
    functor : Functor T
    foldable : Foldable T
    traverse : ∀ {F : Type ℓ → Type ℓ} → IsApplicative F → (a → T b) → T a → F (T b)
    -- tnaturality : ∀ {t} → ApplicativeTransform t . traverse f
