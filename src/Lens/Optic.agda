module Lens.Optic where

open import Common

private
  variable
    s a t b : Type

Optic : (p : Type → Type → Type) → (f : Type → Type) → Type → Type → Type → Type → Type
Optic p f s t a b = p a (f b) -> p s (f t)

Optic' : (p : Type → Type → Type) → (f : Type → Type) → Type → Type → Type
Optic' p f s a = Optic p f s s a a
