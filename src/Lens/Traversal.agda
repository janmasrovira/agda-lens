module Lens.Traversal where

open import Common
open import Classes.Applicative

private
  variable
    s a t b : Type

record Traversal (s : Type) (t : Type) (a : Type) (b : Type) : Type₁ where
  field
    F : Type → Type
    Applicative-F : IsApplicative F
    traversal : (a → F b) → s → F t
