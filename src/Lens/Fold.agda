module Lens.Fold where

open import Common
open import StringUtils
open import Lens.Getter
open import Classes.Contravariant
open import Types.Const
open import Classes.Applicative
open import Function.Endomorphism.Propositional using (Endo)

private
  variable
    r s a : Type

Fold : Type → Type → Type _
Fold s a = Σ[ f ∈ (Type → Type) ] Contravariant f × Applicative f × ((a → f a) → s → f s)

foldMapOf : Getting r s a → (a → r) → s → r
foldMapOf l f = l f

foldrOf : Getting (Endo r) s a → (a → r → r) → r → s → r
foldrOf l f x₀ s = (foldMapOf l f s) x₀

toListOf : Getting (Endo (List a)) s a → s → List a
toListOf l = foldrOf l _∷_ []

_^∙∙_ : {a s : Type} → s → Getting (Endo (List a)) s a → List a
x ^∙∙ l = toListOf l x

idg : Getting (Endo (List a)) a a
idg = id

l1 : List ℕ
l1 = (1 ∷ 2 ∷ [])

x : String
x = show (l1 ^∙∙ id)
