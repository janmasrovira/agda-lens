module Lens.Optical where

open import Common

Optical : (p q : Type → Type → Type) → (f : Type → Type) → Type → Type → Type → Type → Type
Optical p q f s t a b = p a (f b) → q s (f t)

Optical' : (p q : Type → Type → Type) → (f : Type → Type) → Type → Type → Type
Optical' p q f s a = Optical p q f s s a a
