module Lens.Getter where

open import Common
open import Types.Const
open import Classes.Functor
open import Classes.Profunctor
open import Classes.Contravariant

private
  variable
    ℓ ℓa ℓs ℓr : Level
    r s a : Type ℓ

-- s.a → r
Getting : Type → Type → Type → Type
Getting r s a = (a → Const r a) → s → Const r s

Getter : Type → Type → Type _
Getter s a = Σ[ f ∈ (Type → Type) ] Functor f × Contravariant f × ((a → f a) → s → f s)

to : {p : Type → Type → Type} {f : Type → Type} → Profunctor p → Contravariant f
  → (s → a) → p a (f a) → p s (f s)
to p c k = dimap k (contramap k)
  where open Profunctor p
        open Contravariant c

to-Getter : {p : Type → Type → Type} {f : Type → Type} → Profunctor p → Contravariant f
  → (s → a) → p a (f a) → p s (f s)
to-Getter p c k = dimap k (contramap k)
  where open Profunctor p
        open Contravariant c

_^∙_ : s → Getting a s a → a
s ^∙ l = l id s
