module Lens.Setter where

open import Common
open import Types.Identity
open import Function.Structures
open import Classes.Profunctor
open import Lens.Optical

private
  variable
    s a t b : Type
    P Q : Type → Type → Type

ASetter : Type → Type → Type → Type → Type
ASetter s t a b = (a → Identity b) → s → Identity t


-- sets : ((a → b) → s → t) → ASetter s t a b
-- sets = id
sets : Profunctor P → Profunctor Q → (P a b → Q s t) → Optical P Q Identity s t a b
sets _ _ = id

set : ASetter s t a b → b → s → t
set setter b s = setter (const b) s

over : ASetter s t a b → (a → b) → s → t
over setter f s = setter f s

sets-over-IsInverse : (PP : Profunctor P) → (QQ : Profunctor Q)
  → IsInverse _≡_ _≡_ (sets {_} {_} {a} {b} {s} {t} PP QQ) over
sets-over-IsInverse = record {
   isLeftInverse =
   record {
   isCongruent = record { cong = id ;
     isEquivalence₁ = isEquivalence ; isEquivalence₂ = isEquivalence } ;
   cong₂ = id ;
   inverseˡ = λ x → refl } ;
   inverseʳ = λ x → refl }

-- record Setter s t a b : Type₁ where
--   field
--     asetter : ASetter s t a b
--     set-set : ∀ {x y a} → set asetter y (set {!!} {!!} {!!}) ≡ set asetter y a
