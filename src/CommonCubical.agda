module _ where

open import Function using (const; flip; id; _∘_; _∘′_; _$_) public
open import Level using (Level; _⊔_) renaming (zero to ℓ-zero; suc to ℓ-suc; Setω to Typeω) public
open import Agda.Primitive renaming (Set to Type) public
open import Data.List using (List; _∷_; []; map) public
open import Data.Nat hiding (_⊔_) public
open import Relation.Binary.Structures using (IsEquivalence) public
open import Relation.Binary.Definitions public

-- Cubical
open import Cubical.Core.Everything public
open import Cubical.Foundations.Prelude public

private
  variable
    a ℓ : Level
    A : Type ℓ

≡-IsEquivalence : IsEquivalence {_} {ℓ} {A} _≡_
≡-IsEquivalence = record
   { refl = refl
   ; sym = sym
   ; trans = _∙_ }
